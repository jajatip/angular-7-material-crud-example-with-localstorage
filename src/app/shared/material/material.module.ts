import * as Material from "@angular/material";
import { NgModule } from "@angular/core";
 
@NgModule({
    imports: [
        Material.MatGridListModule, 
        Material.MatFormFieldModule,
        Material.MatInputModule,
        Material.MatRadioModule,
        Material.MatDatepickerModule,
        Material.MatNativeDateModule,
        Material.MatSelectModule,
        Material.MatCheckboxModule,
        Material.MatButtonModule,
        Material.MatTableModule,
        Material.MatIconModule,
        Material.MatDialogModule
    ],
    exports: [
        Material.MatGridListModule,
        Material.MatFormFieldModule,
        Material.MatInputModule,
        Material.MatRadioModule,
        Material.MatDatepickerModule,
        Material.MatNativeDateModule,
        Material.MatSelectModule,
        Material.MatCheckboxModule,
        Material.MatButtonModule,
        Material.MatTableModule,
        Material.MatIconModule,
        Material.MatDialogModule
    ]
})
export class MaterialModule { }