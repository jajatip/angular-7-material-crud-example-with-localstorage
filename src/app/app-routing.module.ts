import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users/users.component';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class AppRoutingModule { }

const appRoutes: Routes = [
    { path: '', component: UsersComponent, pathMatch: 'full' }
];
export const routing = RouterModule.forRoot(appRoutes);