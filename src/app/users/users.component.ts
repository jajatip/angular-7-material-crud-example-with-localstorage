import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import {MatDialog } from '@angular/material';
import { User } from './users';
import { AddEditUserComponent } from './add-edit-user/add-edit-user.component';
import { DeleteUserComponent } from './delete-user/delete-user.component';
import { UsersService} from './users.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  dialogRef: any;
  displayedColumns: string[] = ['name', 'url', 'address', 'email', 'age', 'gender', 'dateOfBirth', 'action'];
  dataSource: any;
  users: User[];
  constructor(
    private dialog: MatDialog,
    private usersService: UsersService
  ) {}

  ngOnInit() {
    this.getUsers();
  }

  async getUsers() {
    this.users = await this.usersService.getUsers();
    this.dataSource = new MatTableDataSource(this.users);
  }
  
  Add() {
    let newItem = {
    };
    this.dialogRef = this.dialog.open(AddEditUserComponent, {
      width: '50%',
      disableClose: true
    });

    this.dialogRef.componentInstance.data = newItem;
    this.dialogRef.componentInstance.isAdd = true;
    this.dialogRef.afterClosed().subscribe(result => {
      if(result.isCompleted) {
        this.getUsers();
      }
    });
  }

  edit(user: User) {
    let newdata = Object.assign({}, user);
    this.dialogRef = this.dialog.open(AddEditUserComponent, {
      width: '50%',
      disableClose: true
    });

    this.dialogRef.componentInstance.data = newdata;
    this.dialogRef.componentInstance.isAdd = false;
    this.dialogRef.afterClosed().subscribe(result => {
      if(result.isCompleted) {
        this.getUsers();
      }
    });
  }

  delete(id: any) {
    this.dialogRef = this.dialog.open(DeleteUserComponent, {
      height: '40%',
      width: '40%',
      disableClose: true
    });
    this.dialogRef.componentInstance.data = id;
    this.dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
       this.getUsers();
    });
  }

}


