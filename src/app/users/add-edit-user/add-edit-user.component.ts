import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators  } from '@angular/forms';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { User } from '../users';
import { UsersService } from '../users.service';
import { ValidateUrl } from '../../validator/url.validator';

@Component({
  selector: 'app-add-edit-user',
  templateUrl: './add-edit-user.component.html',
  styleUrls: ['./add-edit-user.component.css']
})
export class AddEditUserComponent implements OnInit {
  registerForm: FormGroup;
  submitted: boolean;
  isEdit: boolean;
  selected = 1;
  constructor(private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    @Inject(MAT_DIALOG_DATA) public isAdd: boolean,
    private usersService: UsersService) {
      this.createForm();
     }

  ngOnInit() {
    
    if (!this.isAdd) {
      this.registerForm.setValue({
        name: this.data.name,
        url: this.data.url,
        address: this.data.address,
        email: this.data.email,
        age: this.data.age,
        gender: this.data.gender ? this.data.gender: 1 ,
        dateOfBirth: this.data.dateOfBirth
      });
    }
  }

   // Create Forms Fields
  createForm() {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      url: ['', [Validators.required, ValidateUrl]],
      age: [0, [Validators.required]],
      dateOfBirth: ['', [Validators.required]],
      gender: [1, [Validators.required] ]
    });
  }

  update() {
    if(!this.data.id) {
      this.add(this.generateUserObject());
    } else {
      this.updateCountry(this.generateUserObject());
    }

  }

  generateUserObject() : User { 
    const user: User = {
      'id': this.data.id ? this.data.id : JSON.parse(localStorage.getItem('users')).length + 1,
      'name': this.registerForm.value.name,
      'url': this.registerForm.value.url,
      'address': this.registerForm.value.address,
      'email': this.registerForm.value.email,
      'age': this.registerForm.value.age,
      'gender': this.registerForm.value.gender,
      'dateOfBirth': this.registerForm.value.dateOfBirth,
    }
    return user;
  }
  close() {
    this.dialogRef.close({isCompleted: false, message: 'User added successfully!'});
  }

  async add(data: User) {
    await this.usersService.addEmployee(data);
    this.dialogRef.close({isCompleted: true, message: 'Country added successfully!'});
  }

  async updateCountry(data: User) {
    await this.usersService.updateEmployee(data);
    this.dialogRef.close({isCompleted: true, message: 'Country updated successfully!'});
  }

}
