export interface User {
    id: number;
    name: string;
    url: string;
    address: string;
    email:string;
    age: number;
    gender: number;
    dateOfBirth:string
  }

