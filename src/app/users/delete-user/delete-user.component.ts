import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { User } from '../users';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})
export class DeleteUserComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: string,
    private usersService: UsersService) { }

  ngOnInit() {
  }

  delete() {
    this.deleteUser(this.data);
  }

  close() {
    this.dialogRef.close({isCompleted: false, message: 'User cancel successfully!'});
  }

  async deleteUser(id: string) {
    await this.usersService.deleteEmployee(id);
    this.dialogRef.close({isCompleted: true, message: 'User deleted successfully!'});
  }

}
