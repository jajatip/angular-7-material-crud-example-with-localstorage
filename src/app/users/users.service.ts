import { Injectable } from '@angular/core';
import { Init } from './users-init';
import { User } from './users';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends Init {

  constructor() {
    super();
    console.log('userservice Works');
    this.load();
  }

  getUsers(): User[] {
    let users = JSON.parse(localStorage.getItem('users'));
    return users;
  }

  addEmployee(user: User) {
    let users = JSON.parse(localStorage.getItem('users'));
    users.push(user);
    localStorage.setItem('users', JSON.stringify(users));
  }

  deleteEmployee(id: string) {
    let users = JSON.parse(localStorage.getItem('users'));
    for (let i = 0; i < users.length; i++) {
      if (users[i].id == id) {
        users.splice(i, 1);
      }
    }
    localStorage.setItem('users', JSON.stringify(users));
  }

  updateEmployee(user: User) {
    let users = JSON.parse(localStorage.getItem('users'));
    for (let i = 0; i < users.length; i++) {
      if (users[i].id == user.id) {
        users[i] = user;
      }
    }
    localStorage.setItem('users', JSON.stringify(users));
  }
}
